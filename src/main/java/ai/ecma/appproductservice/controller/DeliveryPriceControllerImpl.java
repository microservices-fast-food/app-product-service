package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.DeliveryPriceReqDto;
import ai.ecma.appproductservice.payload.DeliveryPriceResDto;
import ai.ecma.appproductservice.service.DeliveryPriceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class DeliveryPriceControllerImpl implements DeliveryPriceController{

    private final DeliveryPriceService deliveryPriceService;

    @Override
    public ApiResult<CustomPage<DeliveryPriceResDto>> getAll(int page, int size) {
        log.info("DeliveryPriceController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<DeliveryPriceResDto>> apiResult = deliveryPriceService.getAll(page, size);
        log.info("DeliveryPriceController getAll resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<DeliveryPriceResDto> getOne(UUID id) {
        log.info("DeliveryPriceController getOne req id :{}", id);
        ApiResult<DeliveryPriceResDto> apiResult = deliveryPriceService.getOne(id);
        log.info("DeliveryPriceController getAll resp ApiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<DeliveryPriceResDto> add(DeliveryPriceReqDto deliveryPriceReqDto) {
        log.info("DeliveryPriceController add req DeliveryPriceReqDto :{}", deliveryPriceReqDto);
        ApiResult<DeliveryPriceResDto> apiResult = deliveryPriceService.add(deliveryPriceReqDto);
        log.info("DeliveryPriceController add resp ApiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<DeliveryPriceResDto> edit(DeliveryPriceReqDto deliveryPriceReqDto, UUID id) {
        log.info("DeliveryPriceController edit req DeliveryPriceReqDto :{}, id:{}", deliveryPriceReqDto, id);
        ApiResult<DeliveryPriceResDto> apiResult = deliveryPriceService.edit(deliveryPriceReqDto, id);
        log.info("DeliveryPriceController edit resp ApiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        log.info("DeliveryPriceController delete req id:{}", id);
        ApiResult<?> apiResult = deliveryPriceService.delete(id);
        log.info("DeliveryPriceController delete resp ApiResult:{}", apiResult);
        return apiResult;
    }
}
