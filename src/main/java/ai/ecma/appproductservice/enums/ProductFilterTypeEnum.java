package ai.ecma.appproductservice.enums;

public enum ProductFilterTypeEnum {
    FAVORITE,
    CATEGORY,
    SALE,
    ACTIVE,
    RECOMMENDED
}
