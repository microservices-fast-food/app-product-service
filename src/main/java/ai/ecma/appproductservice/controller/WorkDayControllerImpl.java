package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.service.WorkDayService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@AllArgsConstructor
public class WorkDayControllerImpl implements WorkDayController{

    private final WorkDayService workDayService;


    @Override
    public ApiResult<CustomPage<WorkDaysResDto>> getAll(int page, int size) {
        return workDayService.getAll(page, size);
    }

    @Override
    public ApiResult<WorkDaysResDto> getOne(UUID id) {
        return workDayService.getOne(id);
    }

    @Override
    public ApiResult<WorkDaysResDto> add(WorkDaysReqDto workDaysReqDto) {
        return workDayService.add(workDaysReqDto);
    }

    @Override
    public ApiResult<WorkDaysResDto> edit(WorkDayEditReqDto workDayEditReqDto, UUID id) {
        return workDayService.edit(workDayEditReqDto, id);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return workDayService.delete(id);
    }
}
