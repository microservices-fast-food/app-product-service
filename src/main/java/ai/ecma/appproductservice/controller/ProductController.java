package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.enums.ProductFilterTypeEnum;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.ProductReqDto;
import ai.ecma.appproductservice.payload.ProductResDto;
import ai.ecma.appproductservice.utils.AppConstant;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RequestMapping(RestConstants.PRODUCT_CONTROLLER)
public interface ProductController {
    @GetMapping
    ApiResult<CustomPage<ProductResDto>> getAll(
            @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size,
            @RequestParam(defaultValue = "name") String field,
            @RequestParam(defaultValue = "true") boolean asc);

    @GetMapping("/filter/{type}")
    ApiResult<CustomPage<ProductResDto>> getFilter(@PathVariable ProductFilterTypeEnum type,
                                                   @RequestParam(required = false) UUID categoryId,
                                                   @RequestParam(required = false) boolean active,
                                                   @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                   @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("/get-products-by-ids")
    ApiResult<List<ProductResDto>> getByIds(@PathVariable Set<UUID> ids);


    @GetMapping("/{id}")
    ApiResult<ProductResDto> getOne(@PathVariable UUID id);

    @PostMapping
    ApiResult<ProductResDto> add(@RequestBody @Valid ProductReqDto productReqDto);

    @PutMapping("/{id}")
    ApiResult<ProductResDto> edit(@PathVariable UUID id, @RequestBody @Valid ProductReqDto productReqDto);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
