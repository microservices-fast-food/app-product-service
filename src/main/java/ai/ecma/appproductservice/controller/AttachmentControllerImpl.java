package ai.ecma.appproductservice.controller;

import ai.ecma.appdblib.entity.product.Attachment;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.service.AttachmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AttachmentControllerImpl implements AttachmentController{

    private final AttachmentService attachmentService;

    @Override
    public ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException {
        log.info("AttachmentController upload req request :{}", request);
        ApiResult<?> apiResult = attachmentService.upload(request);
        log.info("AttachmentController upload resp ApiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CustomPage<Attachment>> getAll(int page, int size) {
        log.info("AttachmentController getAll req page :{}, size:{}", page, size);
        ApiResult<CustomPage<Attachment>> apiResult = attachmentService.getAll(page, size);
        log.info("AttachmentController upload resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<Attachment> getById(UUID id) {
        log.info("AttachmentController grtById req id :{}", id);
        ApiResult<Attachment> apiResult = attachmentService.getById(id);
        log.info("AttachmentController grtById resp ApiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException {
        log.info("AttachmentController getFile req response :{}", response);
        ApiResult<?> apiResult = attachmentService.getFile(id, response);
        log.info("AttachmentController upload resp ApiResult ;{}", apiResult);
        return apiResult;
    }
}
