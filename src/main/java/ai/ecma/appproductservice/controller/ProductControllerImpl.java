package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.enums.ProductFilterTypeEnum;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.ProductReqDto;
import ai.ecma.appproductservice.payload.ProductResDto;
import ai.ecma.appproductservice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ProductControllerImpl implements ProductController{
    private final ProductService productService;


    @Override
    public ApiResult<CustomPage<ProductResDto>> getAll(int page, int size, String field, boolean asc) {
        return productService.getAll(page, size, field, asc);
    }

    @Override
    public ApiResult<CustomPage<ProductResDto>> getFilter(ProductFilterTypeEnum type, UUID categoryId, boolean active, int page, int size) {
        return productService.getFilter(type, categoryId,active, page, size);
    }

    @Override
    public ApiResult<List<ProductResDto>> getByIds(Set<UUID> ids) {
        return productService.getByIds(ids);
    }

    @Override
    public ApiResult<ProductResDto> getOne(UUID id) {
        return productService.getOne(id);
    }

    @Override
    public ApiResult<ProductResDto> add(ProductReqDto productReqDto) {
        return productService.add(productReqDto);
    }

    @Override
    public ApiResult<ProductResDto> edit(UUID id, ProductReqDto productReqDto) {
        return productService.edit(id, productReqDto);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return productService.delete(id);
    }
}
