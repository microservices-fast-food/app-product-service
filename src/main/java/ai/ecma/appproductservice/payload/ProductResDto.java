package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResDto {

    private UUID id;

    private String name;

    private Double price;

    private UUID categoryId;

    private UUID photoId;

    private String photoUrl;

    private String description;

    private UUID saleId;

    private Boolean active;
}
