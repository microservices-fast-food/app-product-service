package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.product.Favorite;
import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.product.FavoriteRepository;
import ai.ecma.appdblib.repository.product.ProductRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FavoriteServiceImpl implements FavoriteService{

    private final FavoriteRepository favoriteRepository;
    private final ProductRepository productRepository;

    private final MessageByLang messageByLang;

    @CheckAuth
    @Override
    public ApiResult<?> add(UUID productId) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));
        User user = CommonUtils.getUserFromRequest();
        boolean existsByUserIdAndProductId = favoriteRepository.existsByUserIdAndProductId(user.getId(), productId);
        if (existsByUserIdAndProductId)
            return ApiResult.successResponse(messageByLang.getMessageByKey("PRODUCT_ADDED_TO_FAVOURITE"));
        Favorite favorite = new Favorite(
                user.getId(),
                product
        );
        favoriteRepository.save(favorite);
        return ApiResult.successResponse(messageByLang.getMessageByKey("PRODUCT_ADDED_TO_FAVOURITE"));
    }

    @CheckAuth
    @Override
    public ApiResult<?> deleteOneFavoriteProduct(UUID productId) {
        try {
            User user = CommonUtils.getUserFromRequest();
            favoriteRepository.deleteByProductIdAndUserId(productId,user.getId());
            return ApiResult.successResponse(messageByLang.getMessageByKey("PRODUCT_DELETED_FROM_FAVOURITE"));
        }catch (Exception e){
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND_IN_FAVOURITE"));
        }
    }

    @CheckAuth
    @Override
    public ApiResult<?> deleteAllFavoriteProduct() {
        try {
            User user = CommonUtils.getUserFromRequest();
            favoriteRepository.deleteAllByUserId(user.getId());
            return ApiResult.successResponse(messageByLang.getMessageByKey("PRODUCTS_DELETED_FROM_FAVOURITE"));
        }catch (Exception e){
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCTS_NOT_FOUND_IN_FAVOURITE"));
        }
    }
}
