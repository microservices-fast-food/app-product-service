package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.DeliveryPriceReqDto;
import ai.ecma.appproductservice.payload.DeliveryPriceResDto;
import ai.ecma.appproductservice.utils.AppConstant;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstants.DELIVERY_PRICE_CONTROLLER)
public interface DeliveryPriceController {

    @GetMapping
    ApiResult<CustomPage<DeliveryPriceResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                     @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("{id}")
    ApiResult<DeliveryPriceResDto> getOne(@PathVariable UUID id);

    @PostMapping
    ApiResult<DeliveryPriceResDto> add(@RequestBody @Valid DeliveryPriceReqDto deliveryPriceReqDto);

    @PutMapping("{id}")
    ApiResult<DeliveryPriceResDto> edit(@RequestBody @Valid DeliveryPriceReqDto deliveryPriceReqDto, @PathVariable UUID id);

    @DeleteMapping("{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
