package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.utils.AppConstant;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstants.WORK_DAY_CONTROLLER)
public interface WorkDayController {

    @GetMapping
    ApiResult<CustomPage<WorkDaysResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                 @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("{id}")
    ApiResult<WorkDaysResDto> getOne(@PathVariable UUID id);

    @PostMapping
    ApiResult<WorkDaysResDto> add(@RequestBody @Valid WorkDaysReqDto workDaysReqDto);

    @PutMapping("{id}")
    ApiResult<WorkDaysResDto> edit(@RequestBody @Valid WorkDayEditReqDto workDayEditReqDto, @PathVariable UUID id);

    @DeleteMapping("{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
