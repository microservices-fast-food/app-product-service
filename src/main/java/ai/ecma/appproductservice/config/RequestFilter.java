package ai.ecma.appproductservice.config;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class RequestFilter implements Filter {
    @Value("${serviceUsername}")
    private String serviceUsernameKey;
    @Value("${servicePassword}")
    private String servicePasswordKey;

    @Value("${service.orderServiceUsername}")
    private String orderServiceUsername;
    @Value("${service.orderServicePassword}")
    private String orderServicePassword;

    @Value("${service.authServiceUsername}")
    private String authServiceUsername;
    @Value("${service.authServicePassword}")
    private String authServicePassword;

    @Value("${service.gatewayServiceUsername}")
    private String gatewayServiceUsername;
    @Value("${service.gatewayServicePassword}")
    private String gatewayServicePassword;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        Gson gson = new Gson();
        HttpServletRequest httpServletRequest =(HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        String serviceUsername = httpServletRequest.getHeader(serviceUsernameKey);
        String servicePassword = httpServletRequest.getHeader(servicePasswordKey);
        String requestURI = httpServletRequest.getRequestURI(); //v2/api-docs
        if(!requestURI.equals("/v2/api-docs")) {
            if (!((orderServiceUsername.equals(serviceUsername) && orderServicePassword.equals(servicePassword)) ||
                    (authServiceUsername.equals(serviceUsername) && authServicePassword.equals(servicePassword)) ||
                    (gatewayServiceUsername.equals(serviceUsername) && gatewayServicePassword.equals(servicePassword))
            )) {
//                throw new RestException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED");
//                ApiResult<ErrorData> errorDataApiResult =ApiResult.errorResponse(new ErrorData("Kirish mumkin emas", 401));
//                httpServletResponse.getWriter().write(gson.toJson(errorDataApiResult));
                httpServletResponse.setStatus(401);
                httpServletResponse.setContentType("application/json");
//                httpServletResponse.sendError(401, "parol qani");
            }
        }

        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }


}
