package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressForBranchDto {

    private UUID id;

    private Double lat;

    private Double lon;

    private String fullAddress;
}
