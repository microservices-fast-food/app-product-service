package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.payload.*;

import java.util.List;
import java.util.UUID;

public interface BranchService {

    ApiResult<CustomPage<BranchResDto>> getAll(int page, int size);

    ApiResult<BranchResDto> getBranch(UUID id);

    ApiResult<BranchResDto> addBranch(BranchReqDto branchReqDto);

    ApiResult<BranchResDto> editBranch(BranchReqDto branchReqDto, UUID id);

    ApiResult<?> deleteBranch(UUID id);

    ApiResult<List<BranchResDto>> getAllByDistrictId();

    ApiResult<List<BranchInfoForClientDto>> findNearestBranchs(FindNearestBranchDto findNearestBranchDto);

    ApiResult<BranchInfoForClientDto> findNearestBranch(FindNearestBranchDto findNearestBranchDto);

}
