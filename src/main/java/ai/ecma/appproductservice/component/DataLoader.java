package ai.ecma.appproductservice.component;

import ai.ecma.appdblib.repository.product.CategoryRepository;
import ai.ecma.appdblib.repository.product.ProductRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {
    private final ProductRepository productRepository;

    @Value("${spring.sql.init.mode}")
    private String dataLoaderMode;

    public DataLoader(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void run(String... args) throws Exception {
       productRepository.uniqueCreator();
        if (dataLoaderMode.equals("always")) {

        }

    }
}
