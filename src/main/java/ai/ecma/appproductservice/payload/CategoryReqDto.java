package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryReqDto {

    @NotBlank(message = "{CATEGORY_NAME_NOT_BLANK}")
    private String name;

    private UUID parentCategoryId;

    private Integer index;
}
