package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.product.Category;
import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appdblib.entity.product.RecommendedProduct;
import ai.ecma.appdblib.repository.product.CategoryRepository;
import ai.ecma.appdblib.repository.product.ProductRepository;
import ai.ecma.appdblib.repository.product.RecommendedProductRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.mapper.CustomMapper;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.RecomendedProductReqDto;
import ai.ecma.appproductservice.payload.RecomendedProductResDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RecommendedProductServiceImpl implements RecommendedProductService {
    private final RecommendedProductRepository recommendedProductRepository;
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
private  final MessageByLang messageByLang;

    @Override
    public ApiResult<CustomPage<RecomendedProductResDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<RecommendedProduct> recommendedProductPage = recommendedProductRepository.findAll(pageable);
        return ApiResult.successResponse(recomendedProductToCustomPage(recommendedProductPage));
    }

    @Override
    public ApiResult<RecomendedProductResDto> getOne(UUID id) {
        RecommendedProduct recommendedProduct = recommendedProductRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND,messageByLang.getMessageByKey("RECOMMENDED_PRODUCT_NOT_FOUND") ));
        return ApiResult.successResponse(CustomMapper.recomendedProductToDto(recommendedProduct));
    }

    @Override
    public ApiResult<List<RecomendedProductResDto>> getByCategory(UUID categoryId) {
        List<RecommendedProduct> recommendedProductList = recommendedProductRepository.findAllByCategoryId(categoryId);
        return ApiResult.successResponse(recommendedProductList.stream().map(CustomMapper::recomendedProductToDto).collect(Collectors.toList()));
    }

    @CheckAuth(permission = PermissionEnum.ADD_RECOMMENDED_PRODUCT)
    @Override
    public ApiResult<RecomendedProductResDto> add(RecomendedProductReqDto recomendedProductReqDto) {
        boolean existsByCategoryIdAndProductId = recommendedProductRepository.existsByCategoryIdAndProductId(recomendedProductReqDto.getCategoryId(), recomendedProductReqDto.getProductId());
        if (existsByCategoryIdAndProductId)
            throw new RestException(HttpStatus.CONFLICT,"Ushbu mahsulot shu kategoriyaga birikirilgan");
        Category category = categoryRepository.findById(recomendedProductReqDto.getCategoryId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CATEGORY_NOT_FOUND")));
        if (category.getParentCategory()==null)
            throw new RestException(HttpStatus.BAD_REQUEST,messageByLang.getMessageByKey("ADDING_PRODUCT_TO_PARENT_CATEGORY_FORBIDDEN"));
        Product product = productRepository.findById(recomendedProductReqDto.getProductId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));

        RecommendedProduct recommendedProduct = new RecommendedProduct(
                category,
                product
        );
        recommendedProductRepository.save(recommendedProduct);
        return ApiResult.successResponse(CustomMapper.recomendedProductToDto(recommendedProduct));
    }

    @CheckAuth(permission = PermissionEnum.DELETE_RECOMMENDED_PRODUCT)
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            recommendedProductRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("RECOMMENDED_PRODUCT_DELETED"));
        }catch (Exception e){
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND,messageByLang.getMessageByKey("RECOMMENDED_PRODUCT_NOT_FOUND"));
        }
    }

    public CustomPage<RecomendedProductResDto> recomendedProductToCustomPage(Page<RecommendedProduct> recommendedProductPage) {
        return new CustomPage<RecomendedProductResDto>(
                recommendedProductPage.getContent().stream().map(CustomMapper::recomendedProductToDto).collect(Collectors.toList()),
                recommendedProductPage.getNumberOfElements(),
                recommendedProductPage.getNumber(),
                recommendedProductPage.getTotalElements(),
                recommendedProductPage.getTotalPages(),
                recommendedProductPage.getSize()
        );
    }
}
