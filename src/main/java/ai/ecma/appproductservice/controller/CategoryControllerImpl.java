package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CategoryReqDto;
import ai.ecma.appproductservice.payload.CategoryResDto;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Slf4j
public class CategoryControllerImpl implements CategoryController{
    private final CategoryService categoryService;

    public CategoryControllerImpl(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public ApiResult<CustomPage<CategoryResDto>> getAll(int page, int size) {
        log.info("CategoryController getAll req page :{}, size:{}", page, size);
        ApiResult<CustomPage<CategoryResDto>> apiResult = categoryService.getAll(page, size);
        log.info("CategoryController getAll resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<CategoryResDto> getOne(UUID id) {
        log.info("CategoryController getOne req id:{}", id);
        ApiResult<CategoryResDto> apiResult = categoryService.getOne(id);
        log.info("CategoryController getOne resp ApiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CategoryResDto> add(CategoryReqDto categoryReqDto) {
        log.info("CategoryController add req CategoryReqDto:{}", categoryReqDto);
        ApiResult<CategoryResDto> apiResult = categoryService.add(categoryReqDto);
        log.info("CategoryController add resp ApiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<CategoryResDto> edit(UUID id, CategoryReqDto categoryReqDto) {
        log.info("CategoryController edit req id:{}, CategoryReqDto:{}", id, categoryReqDto);
        ApiResult<CategoryResDto> apiResult = categoryService.edit(id, categoryReqDto);
        log.info("CategoryController getOne resp ApiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        log.info("CategoryController delete req id:{}", id);
        ApiResult<?> apiResult = categoryService.delete(id);
        log.info("CategoryController delete resp ApiResult:{}", apiResult);
        return apiResult;
    }
}
