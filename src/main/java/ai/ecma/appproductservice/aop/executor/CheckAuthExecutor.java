package ai.ecma.appproductservice.aop.executor;


import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.feign.AuthFeign;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.utils.CommonUtils;
import ai.ecma.appproductservice.utils.RestConstants;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


@Order(value = 1)
@Aspect
@Component
public class CheckAuthExecutor {

    private final AuthFeign authFeign;

    public CheckAuthExecutor(AuthFeign authFeign) {
        this.authFeign = authFeign;
    }

    @Before(value = "@annotation(checkAuth)")
    public void checkAuthExecutor(CheckAuth checkAuth) {
        check(checkAuth);
    }

    public void check(CheckAuth checkAuth) {
        String token = CommonUtils.getTokenFromRequest();
        if (token == null) {
            throw new RestException(HttpStatus.UNAUTHORIZED,"FORBIDDEN");
        }
        User user;
        if (checkAuth.permission().equals(PermissionEnum.CHECK)) {
            ApiResult<User> apiResult = authFeign.checkUser(token);
            user = apiResult.getData();
        } else {
            ApiResult<User> apiResult = authFeign.checkPermission(token, checkAuth.permission());
            user = apiResult.getData();
            if (user == null)
                throw new RestException(HttpStatus.FORBIDDEN,"FORBIDDEN");
        }
        HttpServletRequest httpServletRequest = CommonUtils.currentRequest();
        httpServletRequest.setAttribute(RestConstants.REQUEST_ATTRIBUTE_CURRENT_USER, user);
    }

}
