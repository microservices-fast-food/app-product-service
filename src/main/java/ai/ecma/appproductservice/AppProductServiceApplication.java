package ai.ecma.appproductservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
@EntityScan(basePackages = "ai.ecma.appdblib.entity")                   //db libdagi entitylarni ko'rish uchun qo'yildi
@EnableJpaRepositories(basePackages = "ai.ecma.appdblib.repository")    //db libdagi repositorini ko'rish uchun qo'yildi
public class AppProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppProductServiceApplication.class, args);
    }

}
