package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeliveryPriceResDto {

    private UUID id;

    private Double minPrice; // minKm gacha bo'lgan masofa uchun bo'lgan pul

    private Double minKm; // masalan - 4  km gacha minPrice bor

    private Double priceEveryKm; // minKm dan oshgan dan keyin har bir km uchun hisoblanadigan pul

    private UUID branchId;
}
