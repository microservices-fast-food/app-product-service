package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.payload.ApiResult;

import java.util.UUID;

public interface FavoriteService {

    ApiResult<?> add(UUID productId);

    ApiResult<?> deleteOneFavoriteProduct(UUID productId);

    ApiResult<?> deleteAllFavoriteProduct();

}
