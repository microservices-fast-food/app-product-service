package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.DeliveryPriceReqDto;
import ai.ecma.appproductservice.payload.DeliveryPriceResDto;

import java.util.UUID;

public interface DeliveryPriceService {
    ApiResult<CustomPage<DeliveryPriceResDto>> getAll(int page, int size);

    ApiResult<DeliveryPriceResDto> getOne(UUID id);

    ApiResult<DeliveryPriceResDto> add(DeliveryPriceReqDto deliveryPriceReqDto);

    ApiResult<DeliveryPriceResDto> edit(DeliveryPriceReqDto deliveryPriceReqDto, UUID id);

    ApiResult<?> delete(UUID id);
}
