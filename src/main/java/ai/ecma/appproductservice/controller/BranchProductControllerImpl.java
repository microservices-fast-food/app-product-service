package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.service.BranchProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class BranchProductControllerImpl implements BranchProductController{

    private final BranchProductService branchProductService;

    @Override
    public ApiResult<CustomPage<BranchProductResDto>> getAll(int page, int size) {
       return branchProductService.getAll(page, size);
    }

    @Override
    public ApiResult<BranchProductResDto> getOne(UUID id) {
        return branchProductService.getOne(id);
    }

    @Override
    public ApiResult<BranchProductResDto> add(BranchProductReqDto branchProductReqDto) {
        return branchProductService.add(branchProductReqDto);
    }

    @Override
    public ApiResult<BranchProductResDto> edit(BranchProductEditDto branchProductEditDto, UUID id) {
        return branchProductService.edit(branchProductEditDto, id);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return branchProductService.delete(id);
    }
}
