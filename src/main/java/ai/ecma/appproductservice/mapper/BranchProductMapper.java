package ai.ecma.appproductservice.mapper;

import ai.ecma.appdblib.entity.product.BranchProduct;
import ai.ecma.appproductservice.payload.BranchProductResDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BranchProductMapper {
    // source bu Entity, Target Payload resDto
    @Mapping(source = "branchProduct.branch.id", target = "branchId")
    @Mapping(source = "branchProduct.product.id", target = "productId")
    @Mapping(source = "id", target = "id") //
//    @Mapping(target = "id", ignore = true)
    BranchProductResDto branchProductToDto(BranchProduct branchProduct);
}
