package ai.ecma.appproductservice.payload;

import ai.ecma.appdblib.entity.product.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryResDto {
    private UUID id;
    private String name;
    private Category parentCategory;
    private Integer index;
}
