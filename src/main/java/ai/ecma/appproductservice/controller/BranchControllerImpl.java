package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.service.BranchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class BranchControllerImpl implements BranchController {

    private final BranchService branchService;

    @Override
    public ApiResult<CustomPage<BranchResDto>> getAll(int page, int size) {
        log.info("BranchController getAll req page:{}, size:{}", page, size);
        ApiResult<CustomPage<BranchResDto>> apiResult = branchService.getAll(page, size);
        log.info("BranchController getAll resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<BranchResDto> getBranch(UUID id) {
        log.info("BranchController getBranch req id:{}", id);
        ApiResult<BranchResDto> apiResult = branchService.getBranch(id);
        log.info("BranchController getAll resp ApiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<List<BranchResDto>> getAllByDistrictId() {
        return branchService.getAllByDistrictId();
    }

    @Override
    public ApiResult<BranchResDto> add(BranchReqDto branchReqDto) {
        log.info("BranchController add req BranchReqDto : {}", branchReqDto);
        ApiResult<BranchResDto> apiResult = branchService.addBranch(branchReqDto);
        log.info("BranchController add resp ApiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<BranchResDto> edit(BranchReqDto branchReqDto, UUID id) {
        log.info("BranchController edit req BranchReqDto :{}, id :{}", branchReqDto, id);
        ApiResult<BranchResDto> apiResult = branchService.editBranch(branchReqDto, id);
        log.info("BranchController edit resp ApiResult :{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        log.info("BranchController delete req id :{}", id);
        ApiResult<?> apiResult = branchService.deleteBranch(id);
        log.info("BranchController getAll resp : success");
        return apiResult;
    }

    @Override
    public ApiResult<List<BranchInfoForClientDto>> findNearestBranchs(FindNearestBranchDto findNearestBranchDto) {
        return branchService.findNearestBranchs(findNearestBranchDto);
    }

    @Override
    public ApiResult<BranchInfoForClientDto> findNearestBranch(FindNearestBranchDto findNearestBranchDto) {
        return branchService.findNearestBranch(findNearestBranchDto);
    }
}
