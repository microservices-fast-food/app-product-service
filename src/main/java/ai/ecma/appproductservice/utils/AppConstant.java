package ai.ecma.appproductservice.utils;

public interface AppConstant {
    String DEFAULT_PAGE_NUMBER = "0";
    String DEFAULT_PAGE_SIZE = "20";
}
