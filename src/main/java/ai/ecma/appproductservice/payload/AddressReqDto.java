package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressReqDto {

    @NotNull(message ="{LAT_VALUE_NOT_NULL}")
    private Double lat;

    @NotNull(message = "{LON_VALUE_NOT_NULL}")
    private Double lon;

    @NotBlank(message = "{FULL_ADDRESS_NOT_BLANK}")
    private String fullAddress;

    @NotNull(message = "{DISTRICT_ID_NOT_NULL}")
    private UUID districtId;
}
