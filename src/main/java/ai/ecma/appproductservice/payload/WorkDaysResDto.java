package ai.ecma.appproductservice.payload;

import ai.ecma.appdblib.entity.enums.WeekdayEnum;
import ai.ecma.appdblib.entity.product.Branch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkDaysResDto {

    private UUID id;

    private Branch branch;

    private WeekdayEnum weekday;

    private Time startTime;

    private Time endTime;
}
