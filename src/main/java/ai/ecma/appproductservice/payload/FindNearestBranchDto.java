package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor

//clientga eng yaqin branch larni topish uchun product service ga beriladigan class
public class FindNearestBranchDto {
    private UUID addressId;
    private Set<UUID> productIds;
    private Timestamp planReceivedTime;
}
