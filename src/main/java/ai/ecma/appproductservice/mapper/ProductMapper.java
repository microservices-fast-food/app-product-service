package ai.ecma.appproductservice.mapper;

import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appproductservice.payload.ProductResDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    List<ProductResDto> listProductToDto(List<Product> productList);
}
