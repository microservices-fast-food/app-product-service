package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.SaleReqDTO;
import ai.ecma.appproductservice.payload.SaleResDTO;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstants.SALE_CONTROLLER)
public interface SaleController {

    @GetMapping
    ApiResult<List<SaleResDTO>> getAll();

    @GetMapping("/{id}")
    ApiResult<SaleResDTO> getOne(@PathVariable UUID id);

    @GetMapping("createdAt")
    ApiResult<List<SaleResDTO>> getCreateAt();

    @PostMapping
    ApiResult<SaleResDTO> add(@Valid @RequestBody SaleReqDTO saleReqDTO);

    @PutMapping("/{id}")
    ApiResult<SaleResDTO> edit(@Valid @RequestBody SaleReqDTO saleReqDTO, @PathVariable UUID id);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);

}
