package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.product.Branch;
import ai.ecma.appdblib.entity.product.BranchProduct;
import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appdblib.repository.product.BranchProductRepository;
import ai.ecma.appdblib.repository.product.BranchRepository;
import ai.ecma.appdblib.repository.product.ProductRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.mapper.BranchProductMapper;
import ai.ecma.appproductservice.payload.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BranchProductServiceImpl implements BranchProductService{

    private final BranchProductRepository branchProductRepository;
    private final BranchRepository branchRepository;
    private final ProductRepository productRepository;
    private final BranchProductMapper branchProductMapper;
    private final MessageByLang messageByLang;

    @CheckAuth(permission = PermissionEnum.VIEW_BRANCH_PRODUCT)
    @Override
    public ApiResult<CustomPage<BranchProductResDto>> getAll(int page, int size) {
        Pageable pageable= PageRequest.of(page, size);
        Page<BranchProduct> branchProductPage = branchProductRepository.findAll(pageable);
        CustomPage<BranchProductResDto> branchProductResDtoCustomPage=branchProductResDtoCustomPage(branchProductPage);
        return ApiResult.successResponse(branchProductResDtoCustomPage);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_BRANCH_PRODUCT)
    @Override
    public ApiResult<BranchProductResDto> getOne(UUID id) {
        BranchProduct branchProduct = branchProductRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BRANCH_PRODUCT_NOT_FOUND")));
        return ApiResult.successResponse(branchProductMapper.branchProductToDto(branchProduct));
    }

    @CheckAuth(permission = PermissionEnum.ADD_BRANCH_PRODUCT)
    @Override
    public ApiResult add(BranchProductReqDto branchProductReqDto) {
        List<Branch> branchList = branchRepository.findAllById(branchProductReqDto.getBranchIdList());
        List<Product> productList = productRepository.findAllById(branchProductReqDto.getProductIdList());
        if (branchList.size()!=branchProductReqDto.getBranchIdList().size()||
                productList.size()!=branchProductReqDto.getProductIdList().size()) {
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("BRANCH_OR_PRODUCT_ERROR"));
        }
        List<BranchProduct> branchProductList=new ArrayList<>();
        for (Branch branch : branchList) {
            for (Product product : productList) {
                BranchProduct branchProduct=new BranchProduct(
                        branch,
                        product,
                        branchProductReqDto.getActive()
                );
                branchProductList.add(branchProduct);
            }
        }
        branchProductRepository.saveAll(branchProductList);
        return ApiResult.successResponse(messageByLang.getMessageByKey("BRANCH_PRODUCT_ADDED"));
    }

    @CheckAuth(permission = PermissionEnum.EDIT_BRANCH_PRODUCT)
    @Override
    public ApiResult<BranchProductResDto> edit(BranchProductEditDto branchProductEditDto, UUID id) {
        BranchProduct branchProduct = branchProductRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BRANCH_PRODUCT_NOT_FOUND")));
        branchProduct.setActive(branchProductEditDto.getActive());
        branchProductRepository.save(branchProduct);
        return ApiResult.successResponse(branchProductMapper.branchProductToDto(branchProduct));
    }

    @CheckAuth(permission = PermissionEnum.DELETE_BRANCH_PRODUCT)
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            branchProductRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("BRANCH_PRODUCT_DELETED"));
        }catch (Exception ex){
            ex.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BRANCH_PRODUCT_NOT_FOUND"));
        }
    }

    public CustomPage<BranchProductResDto> branchProductResDtoCustomPage(Page<BranchProduct> branchProductPage){
        return new CustomPage<>(
                branchProductPage.getContent().stream().map(branchProductMapper::branchProductToDto).collect(Collectors.toList()),
                branchProductPage.getTotalPages(),
                branchProductPage.getNumber(),
                branchProductPage.getTotalElements(),
                branchProductPage.getSize(),
                branchProductPage.getNumberOfElements()
        );
    }
}
