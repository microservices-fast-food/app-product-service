package ai.ecma.appproductservice.feign;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appproductservice.config.FeignConfig;
import ai.ecma.appproductservice.payload.AddressForBranchDto;
import ai.ecma.appproductservice.payload.AddressReqDto;
import ai.ecma.appproductservice.payload.AddressResDto;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@FeignClient(name = RestConstants.AUTH_SERVICE, configuration = FeignConfig.class)
public interface AuthFeign {

    @GetMapping("/auth/check-user")
    ApiResult<User> checkUser(@RequestHeader("Authorization") String token);

    @PostMapping("/auth/check-permission")
    ApiResult<User> checkPermission(@RequestHeader("Authorization") String token,
                                    @RequestBody PermissionEnum permissionEnum);

    /**
     * Authga borib addressni beramiz, u bizga addressni saqlab
     * saqlangan addressni qaytaradi.(Todo yo'lni yozish kerak)
     */

    @PostMapping("/address")
    ApiResult<AddressResDto> saveAddress(@RequestBody AddressReqDto addressReqDto);

    @PutMapping("/address/{id}")
    ApiResult<AddressResDto> editAddress(@RequestBody AddressReqDto addressReqDto,
                                         @PathVariable(name = "id") UUID id);

    @GetMapping("/user/{id}")
    ApiResult<User> getUserById(@PathVariable(name = "id") UUID id);

    @PostMapping("/get-addresses-by-ids")
    ApiResult<List<AddressForBranchDto>> getAddressesByIds(@RequestBody Set<UUID> ids);

    @GetMapping("/{id}")
    ApiResult<AddressForBranchDto> getOneAddress(@PathVariable(name = "id") UUID id);


}
