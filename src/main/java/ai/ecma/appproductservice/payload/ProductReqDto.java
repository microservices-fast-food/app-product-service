package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductReqDto {
    @NotBlank(message = "{PRODUCT_NAME_NOT_BLANK}")
    private String name;

    @Positive(message = "{PRODUCT_PRICE_POSITIVE}")
    private Double price;

    @NotNull(message = "{CATEGORY_ID_NOT_NULL}")
    private UUID categoryId;

    @NotNull(message = "{ATTACHMENT_ID_NOT_NULL}")
    private UUID photoId;

    private String description;

    private UUID saleId;

    @NotNull(message = "{ACTIVE_VALUE_NOT_NULL}")
    private Boolean active;
}
