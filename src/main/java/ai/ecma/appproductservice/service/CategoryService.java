package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CategoryReqDto;
import ai.ecma.appproductservice.payload.CategoryResDto;
import ai.ecma.appproductservice.payload.CustomPage;

import java.util.UUID;

public interface CategoryService {
    ApiResult<CustomPage<CategoryResDto>> getAll(int page, int size);

    ApiResult<CategoryResDto> getOne(UUID id);

    ApiResult<CategoryResDto> add(CategoryReqDto categoryReqDto);

    ApiResult<CategoryResDto> edit(UUID id, CategoryReqDto categoryReqDto);

    ApiResult<?> delete(UUID id);
}
