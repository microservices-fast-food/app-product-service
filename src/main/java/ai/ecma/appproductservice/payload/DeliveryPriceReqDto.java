package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeliveryPriceReqDto {

    @NotNull(message = "{MIN_PRICE_NOT_NULL}")
    private Double minPrice;

    @NotNull(message = "{MIN_KM_NOT_NULL}")
    private Double minKm;

    @NotNull(message = "{PRICE_EVERY_KM_NOT_NULL}")
    private Double priceEveryKm;

    @NotNull(message = "{BRANCH_ID_NOT_NULL}")
    private UUID branchId;
}
