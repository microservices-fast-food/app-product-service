package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


@RequestMapping(RestConstants.FAVORITE_CONTROLLER)
public interface FavoriteController {

    @PostMapping
    ApiResult<?> add(@RequestBody UUID productId);

    @DeleteMapping("/{productId}")
    ApiResult<?> deleteOneFavoriteProduct(@PathVariable UUID productId);

    @DeleteMapping()
    ApiResult<?> deleteAllFavoriteProduct();
}
