package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.payload.*;

import java.util.UUID;

public interface BranchProductService {

    ApiResult<CustomPage<BranchProductResDto>> getAll(int page, int size);

    ApiResult<BranchProductResDto> getOne(UUID id);

    ApiResult<BranchProductResDto> add(BranchProductReqDto branchProductReqDto);

    ApiResult<BranchProductResDto> edit(BranchProductEditDto branchProductEditDto, UUID id);

    ApiResult<?> delete(UUID id);
}
