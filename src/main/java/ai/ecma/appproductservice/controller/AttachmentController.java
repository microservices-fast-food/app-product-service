package ai.ecma.appproductservice.controller;

import ai.ecma.appdblib.entity.product.Attachment;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.utils.AppConstant;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RequestMapping(RestConstants.ATTACHMENT_CONTROLLER)
public interface AttachmentController {

    @PostMapping("/upload")
    ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException;

    @GetMapping("/info")
    ApiResult<CustomPage<Attachment>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                             @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("/info/{id}")
    ApiResult<Attachment> getById(@PathVariable UUID id);


    @GetMapping("/download/{id}")
    ApiResult<?> getFile(@PathVariable UUID id, HttpServletResponse response) throws IOException;


}
