package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.product.Branch;
import ai.ecma.appdblib.entity.product.DeliveryPrice;
import ai.ecma.appdblib.repository.product.BranchRepository;
import ai.ecma.appdblib.repository.product.DeliveryPriceRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.mapper.CustomMapper;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.DeliveryPriceReqDto;
import ai.ecma.appproductservice.payload.DeliveryPriceResDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DeliveryPriceServiceImpl implements DeliveryPriceService{

    private final DeliveryPriceRepository deliveryPriceRepository;
    private final BranchRepository branchRepository;
    private final MessageByLang messageByLang;

    @CheckAuth(permission = PermissionEnum.VIEW_DELIVERY_PRICE)
    @Override
    public ApiResult<CustomPage<DeliveryPriceResDto>> getAll(int page, int size) {
        Pageable pageable= PageRequest.of(page, size);
        Page<DeliveryPrice> deliveryPricePage = deliveryPriceRepository.findAll(pageable);
        CustomPage<DeliveryPriceResDto> customPage=deliveryPriceResDtoCustomPage(deliveryPricePage);
        return ApiResult.successResponse(customPage);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_DELIVERY_PRICE)
    @Override
    public ApiResult<DeliveryPriceResDto> getOne(UUID id) {
        DeliveryPrice deliveryPrice = deliveryPriceRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("DELIVERY_PRICE_NOT_FOUND")));
        return ApiResult.successResponse(CustomMapper.deliveryPriceResToDoDto(deliveryPrice));
    }

    @CheckAuth(permission = PermissionEnum.ADD_DELIVERY_PRICE)
    @Override
    public ApiResult<DeliveryPriceResDto> add(DeliveryPriceReqDto deliveryPriceReqDto) {
        Branch branch = branchRepository.findById(deliveryPriceReqDto.getBranchId()).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BRANCH_NOT_FOUND")));
        DeliveryPrice deliveryPrice=new DeliveryPrice(
                deliveryPriceReqDto.getMinPrice(),
                deliveryPriceReqDto.getMinKm(),
                deliveryPriceReqDto.getPriceEveryKm(),
                branch
        );
        deliveryPriceRepository.save(deliveryPrice);
        return ApiResult.successResponse(CustomMapper.deliveryPriceResToDoDto(deliveryPrice));
    }

    @CheckAuth(permission = PermissionEnum.EDIT_DELIVERY_PRICE)
    @Override
    public ApiResult<DeliveryPriceResDto> edit(DeliveryPriceReqDto deliveryPriceReqDto, UUID id) {
        DeliveryPrice deliveryPrice = deliveryPriceRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("DELIVERY_PRICE_NOT_FOUND")));
        Branch branch = branchRepository.findById(deliveryPriceReqDto.getBranchId()).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BRANCH_NOT_FOUND")));
        deliveryPrice.setMinPrice(deliveryPriceReqDto.getMinPrice());
        deliveryPrice.setPriceEveryKm(deliveryPriceReqDto.getPriceEveryKm());
        deliveryPrice.setMinKm(deliveryPriceReqDto.getMinKm());
        deliveryPrice.setBranch(branch);
        deliveryPriceRepository.save(deliveryPrice);
        return ApiResult.successResponse(CustomMapper.deliveryPriceResToDoDto(deliveryPrice));
    }

    @CheckAuth(permission = PermissionEnum.DELETE_DELIVERY_PRICE)
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            deliveryPriceRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("DELIVERY_PRICE_DELETED"));
        }catch (Exception e){
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("DELIVERY_PRICE_NOT_FOUND"));
        }
    }

    public CustomPage<DeliveryPriceResDto> deliveryPriceResDtoCustomPage(Page<DeliveryPrice> deliveryPricePage){
        return new CustomPage<>(
                deliveryPricePage.getContent().stream().map(CustomMapper::deliveryPriceResToDoDto).collect(Collectors.toList()),
                deliveryPricePage.getTotalPages(),
                deliveryPricePage.getNumber(),
                deliveryPricePage.getTotalElements(),
                deliveryPricePage.getSize(),
                deliveryPricePage.getNumberOfElements()
        );
    }
}
