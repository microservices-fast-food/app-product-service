package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.product.Attachment;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public interface AttachmentService {


    ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException;

    ApiResult<CustomPage<Attachment>> getAll(int page, int size);

    ApiResult<Attachment> getById(UUID id);

    ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException;
}
