package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.SaleReqDTO;
import ai.ecma.appproductservice.payload.SaleResDTO;
import ai.ecma.appproductservice.service.SaleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class SaleControllerImpl implements SaleController{

    private final SaleService saleService;

    @Override
    public ApiResult<List<SaleResDTO>> getAll() {
        return saleService.getAll();
    }

    @Override
    public ApiResult<SaleResDTO> getOne(UUID id) {
        return saleService.getOne(id);
    }

    @Override
    public ApiResult<List<SaleResDTO>> getCreateAt() {
        return saleService.getCreatedAt();
    }

    @Override
    public ApiResult<SaleResDTO> add(SaleReqDTO saleReqDTO) {
        return saleService.add(saleReqDTO);
    }

    @Override
    public ApiResult<SaleResDTO> edit(SaleReqDTO saleReqDTO, UUID id) {
        return saleService.edit(saleReqDTO,id);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return saleService.delete(id);
    }
}
