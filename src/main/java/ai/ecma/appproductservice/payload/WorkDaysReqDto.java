package ai.ecma.appproductservice.payload;

import ai.ecma.appdblib.entity.enums.WeekdayEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkDaysReqDto {

    @NotNull(message = "{BRANCH_ID_NOT_NULL}")
    private List<UUID> branchIdList;

    @NotNull(message = "{WEEKDAY_ENUM_NOT_BLANK}")
    private Set<WeekdayEnum> weekdayList;

    @NotNull(message = "{START_TIME_NOT_NULL}")
    private Time startTime;

    @NotNull(message = "{END_TIME_NOT_NULL}")
    private Time endTime;
}
