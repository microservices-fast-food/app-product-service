package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.service.FavoriteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@Slf4j
public class FavoriteControllerImpl implements FavoriteController {

    private final FavoriteService favoriteService;

    @Override
    public ApiResult<?> add(UUID productId) {
        log.info("FavoriteController add req productId:{}", productId);
        ApiResult<?> apiResult = favoriteService.add(productId);
        log.info("FavoriteController add resp ApiResult:{}", apiResult);
        return apiResult;
    }

    @Override
    public ApiResult<?> deleteOneFavoriteProduct(UUID productId) {
        log.info("FavoriteController deleteOneFavoriteProduct req productId:{}", productId);
        ApiResult<?> apiResult = favoriteService.deleteOneFavoriteProduct(productId);
        return apiResult;
    }

    @Override
    public ApiResult<?> deleteAllFavoriteProduct() {
        return favoriteService.deleteAllFavoriteProduct();
    }
}
