package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.payload.*;

import java.util.UUID;

public interface WorkDayService {

    ApiResult<CustomPage<WorkDaysResDto>> getAll(int page, int size);

    ApiResult<WorkDaysResDto> getOne(UUID id);

    ApiResult<WorkDaysResDto> add(WorkDaysReqDto workDaysReqDto);

    ApiResult<WorkDaysResDto> edit(WorkDayEditReqDto workDayEditReqDto, UUID id);

    ApiResult<?> delete(UUID id);
}
