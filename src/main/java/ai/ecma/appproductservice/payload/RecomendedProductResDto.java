package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecomendedProductResDto {
    private UUID id;
    private UUID categoryId;
    private ProductResDto productResDto;
}
