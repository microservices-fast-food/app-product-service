package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.product.Sale;
import ai.ecma.appdblib.repository.product.SaleRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.mapper.CustomMapper;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.SaleReqDTO;
import ai.ecma.appproductservice.payload.SaleResDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SaleServiceImpl implements SaleService{

    private  final SaleRepository saleRepository;
    private final MessageByLang messageByLang;

    @CheckAuth(permission = PermissionEnum.VIEW_SALE)
    @Override
    public ApiResult<List<SaleResDTO>> getAll() {
        List<Sale> saleList = saleRepository.findAll();
        List<SaleResDTO> saleResDTOList=new ArrayList<>();
        for (Sale sale : saleList) {
            saleResDTOList.add(CustomMapper.saleResToDoDTO(sale));
        }
        return ApiResult.successResponse(saleResDTOList);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_SALE)
    @Override
    public ApiResult<SaleResDTO> getOne(UUID id) {
        Sale sale = saleRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("SALE_NOT_FOUND") ));
        return ApiResult.successResponse(CustomMapper.saleResToDoDTO(sale));
    }

    @CheckAuth(permission = PermissionEnum.VIEW_SALE)
    @Override
    public ApiResult<List<SaleResDTO>> getCreatedAt() {
        List<Sale> allByCreatedAtOrderByDesc = saleRepository.getAllByCreatedAtOrderByDesc();
        List<SaleResDTO> saleResDTOList=new ArrayList<>();
        for (Sale sale : allByCreatedAtOrderByDesc) {
            saleResDTOList.add(CustomMapper.saleResToDoDTO(sale));
        }
        return ApiResult.successResponse(saleResDTOList);
    }

    @CheckAuth(permission = PermissionEnum.ADD_SALE)
    @Override
    public ApiResult<SaleResDTO> add(SaleReqDTO saleReqDTO) {
        Sale sale=new Sale(
                saleReqDTO.getName(),
                saleReqDTO.getType(),
                saleReqDTO.getActive(),
                saleReqDTO.getAmount()
        );
        Sale save = saleRepository.save(sale);
        return ApiResult.successResponse(CustomMapper.saleResToDoDTO(save));
    }

    @CheckAuth(permission = PermissionEnum.EDIT_SALE)
    @Override
    public ApiResult<SaleResDTO> edit(SaleReqDTO saleReqDTO, UUID id) {
        Sale sale = saleRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("SALE_NOT_FOUND")));
        sale.setName(saleReqDTO.getName());
        sale.setActive(saleReqDTO.getActive());
        sale.setType(saleReqDTO.getType());
        sale.setAmount(saleReqDTO.getAmount());
        Sale save = saleRepository.save(sale);

        return ApiResult.successResponse(CustomMapper.saleResToDoDTO(save));
    }

    @CheckAuth(permission = PermissionEnum.DELETE_SALE)
    @Override
    public ApiResult<?> delete(UUID id) {
        try{
            saleRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("SALE_DELETED"));
        }catch (Exception ex){
            ex.printStackTrace();
            throw  new RestException(HttpStatus.NOT_FOUND,messageByLang.getMessageByKey("SALE_NOT_FOUND"));
        }
    }
}
