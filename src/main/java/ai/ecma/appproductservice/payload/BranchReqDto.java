package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BranchReqDto {

    @NotBlank(message = "{BRANCH_NAME_NOT_BLANK}")
    private String name;

    @NotNull(message = "{BRANCH_ADDRESS_NOT_NULL}")
    private AddressReqDto addressReqDto;

    @NotNull(message = "{ACTIVE_VALUE_NOT_NULL}")
    private Boolean active;

    @Positive(message = "{COOKING_TIME_POSITIVE}")
    private Integer cookingTime;

    @NotNull(message = "{AUTO_DELIVERED_VALUE_NOT_NULL}")
    private Boolean autoDelivered;

    @Positive(message = "{DELIVERY_MAX_KM_VALUE_POSITIVE}")
    private Double deliveryMaxKm;
}
