package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.RecomendedProductReqDto;
import ai.ecma.appproductservice.payload.RecomendedProductResDto;
import ai.ecma.appproductservice.service.RecommendedProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class RecommendedProductImpl implements RecommendedProductController {
    private final RecommendedProductService recommendedProductService;

    @Override
    public ApiResult<CustomPage<RecomendedProductResDto>> getAll(int page, int size) {
        return recommendedProductService.getAll(page,size);
    }

    @Override
    public ApiResult<RecomendedProductResDto> getOne(UUID id) {
        return recommendedProductService.getOne(id);
    }

    @Override
    public ApiResult<List<RecomendedProductResDto>> getByCategory(UUID categoryId) {
        return recommendedProductService.getByCategory(categoryId);
    }

    @Override
    public ApiResult<RecomendedProductResDto> add(RecomendedProductReqDto recomendedProductReqDto) {
        return recommendedProductService.add(recomendedProductReqDto);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return recommendedProductService.delete(id);
    }
}
