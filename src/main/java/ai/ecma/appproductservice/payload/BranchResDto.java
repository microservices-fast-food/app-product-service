package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BranchResDto {

    private UUID id;

    private String name;

    private AddressResDto addressResDto;

    private Boolean active=false;

    private Integer cookingTime;

    private Boolean autoDelivered;

    private Double deliveryMaxKm;
}
