package ai.ecma.appproductservice.utils;

public interface RestConstants {
    String BASE_PATH = "/api/open-product";
    String DOMAIN = "http://localhost";
    String CATEGORY_CONTROLLER =  BASE_PATH + "/category";
    String FAVORITE_CONTROLLER =  BASE_PATH + "/favorite";
    String PRODUCT_CONTROLLER =  BASE_PATH + "/product";
    String RECOMMENDED_PRODUCT_CONTROLLER =  BASE_PATH + "/recommended-product";

    String AUTH_SERVICE = "AUTH-SERVICE/api/open-auth";
    String REQUEST_ATTRIBUTE_CURRENT_USER = "user";
    String ATTACHMENT_CONTROLLER=BASE_PATH+"/attachment";
    String SALE_CONTROLLER=BASE_PATH+"/sale";
    String BRANCH_CONTROLLER = BASE_PATH + "/branch";
    String DELIVERY_PRICE_CONTROLLER = BASE_PATH + "/delivery";
    String BRANCH_PRODUCT_CONTROLLER = BASE_PATH + "/branch-product";
    String WORK_DAY_CONTROLLER = BASE_PATH + "/workday";
    String ORDER_SERVICE = "ORDER-SERVICE/api/open-order";
    String ORDER_CONTROLLER = ORDER_SERVICE+"/order";
}
