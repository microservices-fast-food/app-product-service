package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.enums.ProductFilterTypeEnum;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.ProductReqDto;
import ai.ecma.appproductservice.payload.ProductResDto;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface ProductService {
    ApiResult<CustomPage<ProductResDto>> getAll(int page, int size, String field, boolean asc);

    ApiResult<CustomPage<ProductResDto>> getFilter(ProductFilterTypeEnum type, UUID categoryId, boolean active, int page, int size);

    ApiResult<ProductResDto> getOne(UUID id);

    ApiResult<ProductResDto> add(ProductReqDto productReqDto);

    ApiResult<ProductResDto> edit(UUID id, ProductReqDto productReqDto);

    ApiResult<?> delete(UUID id);

    ApiResult<List<ProductResDto>> getByIds(Set<UUID> ids);
}
