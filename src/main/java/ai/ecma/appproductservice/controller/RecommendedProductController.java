package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.RecomendedProductReqDto;
import ai.ecma.appproductservice.payload.RecomendedProductResDto;
import ai.ecma.appproductservice.utils.AppConstant;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstants.RECOMMENDED_PRODUCT_CONTROLLER)
public interface RecommendedProductController {
    @GetMapping
    ApiResult<CustomPage<RecomendedProductResDto>> getAll(
            @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("/{id}")
    ApiResult<RecomendedProductResDto> getOne(@PathVariable UUID id);

    @GetMapping("/byCategory/{categoryId}")
    ApiResult<List<RecomendedProductResDto>> getByCategory(@PathVariable UUID categoryId);

    @PostMapping
    ApiResult<RecomendedProductResDto> add(@RequestBody @Valid RecomendedProductReqDto recomendedProductReqDto);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
