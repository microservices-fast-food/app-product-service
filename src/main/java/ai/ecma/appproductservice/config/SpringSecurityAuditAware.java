package ai.ecma.appproductservice.config;

import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appproductservice.utils.CommonUtils;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;
import java.util.UUID;

public class SpringSecurityAuditAware implements AuditorAware<UUID> {
    @Override
    public Optional<UUID> getCurrentAuditor() {
        User user = CommonUtils.getUserFromRequest();
        if (user != null ) {
            return Optional.ofNullable(user.getId());
        }

        return Optional.empty();
    }
}
