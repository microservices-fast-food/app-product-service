package ai.ecma.appproductservice.mapper;


import ai.ecma.appdblib.entity.product.*;
import ai.ecma.appdblib.entity.user.Address;
import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.utils.CommonUtils;


public class CustomMapper {
    public static CategoryResDto categoryToDto(Category category) {
        return new CategoryResDto(
                category.getId(),
                category.getName(),
                category.getParentCategory(),
                category.getIndex());
    }

    public static BranchResDto branchResToDoDto(Branch branch) {
        return new BranchResDto(
                branch.getId(),
                branch.getName(),
                branch.getAddress() != null
                        ? addressResToDoDto(branch.getAddress())
                        : null,
                branch.getActive(),
                branch.getCookingTime(),
                branch.getAutoDelivery(),
                branch.getDeliveryMaxKm()
        );
    }
    public static DeliveryPriceResDto deliveryPriceResToDoDto(DeliveryPrice deliveryPrice){
        return new DeliveryPriceResDto(
                deliveryPrice.getId(),
                deliveryPrice.getMinPrice(),
                deliveryPrice.getMinKm(),
                deliveryPrice.getPriceEveryKm(),
                deliveryPrice.getBranch().getId()!=null
                        ? deliveryPrice.getBranch().getId()
                        : null
        );
    }

    public static AddressResDto addressResToDoDto(Address address) {
        return new AddressResDto(
                address.getId(),
                address.getLon(),
                address.getLat(),
                address.getFullAddress(),
                address.getDistrict().getId()
        );
    }

    public static SaleResDTO saleResToDoDTO(Sale sale) {
        return new SaleResDTO(
                sale.getId(),
                sale.getName(),
                sale.getType(),
                sale.getActive(),
                sale.getAmount()
        );
    }

    public static ProductResDto productToDto(Product product) {
        return new ProductResDto(
                product.getId(),
                product.getName(),
                product.getPrice(),
                product.getCategory() != null ? product.getCategory().getId()
                        : null,
                product.getPhoto() != null ? product.getPhoto().getId()
                        : null,
                product.getPhoto() != null ? CommonUtils.buildPhotoUrl(product.getPhoto().getId())
                        : null,
                product.getDescription(),
                product.getSale() != null ? product.getSale().getId()
                        : null,
                product.getActive()
        );
    }

    public static RecomendedProductResDto recomendedProductToDto(RecommendedProduct recommendedProduct) {
        return new RecomendedProductResDto(
                recommendedProduct.getId(),
                recommendedProduct.getCategory() != null ? recommendedProduct.getCategory().getId()
                        : null,
                recommendedProduct.getProduct() != null ? productToDto(recommendedProduct.getProduct())
                        : null
        );
    }

    public static BranchProductResDto branchProductToDto(BranchProduct branchProduct){
        return new BranchProductResDto(
                branchProduct.getId(),
                branchProduct.getBranch()!=null
                        ?branchProduct.getBranch().getId()
                        :null,
                branchProduct.getProduct()!=null
                        ?branchProduct.getProduct().getId()
                        :null,
                branchProduct.getActive()
        );
    }

    public static WorkDaysResDto workDaysToDto(WorkDay workDay){
        return new WorkDaysResDto(
                workDay.getId(),
                workDay.getBranch()!=null
                ?workDay.getBranch()
                        :null,
                workDay.getWeekday(),
                workDay.getStartTime(),
                workDay.getEndTime()
        );
    }
}
