package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.SaleReqDTO;
import ai.ecma.appproductservice.payload.SaleResDTO;

import java.util.List;
import java.util.UUID;

public interface SaleService {
    ApiResult<List<SaleResDTO>> getAll();

    ApiResult<SaleResDTO> getOne(UUID id);

    ApiResult<List<SaleResDTO>> getCreatedAt();

    ApiResult<SaleResDTO> add(SaleReqDTO saleReqDTO);

    ApiResult<SaleResDTO> edit(SaleReqDTO saleReqDTO, UUID id);

    ApiResult<?> delete(UUID id);
}
