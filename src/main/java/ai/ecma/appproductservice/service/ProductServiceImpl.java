package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.enums.SaleTypeEnum;
import ai.ecma.appdblib.entity.product.Attachment;
import ai.ecma.appdblib.entity.product.Category;
import ai.ecma.appdblib.entity.product.Product;
import ai.ecma.appdblib.entity.product.Sale;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.product.AttachmentRepository;
import ai.ecma.appdblib.repository.product.CategoryRepository;
import ai.ecma.appdblib.repository.product.ProductRepository;
import ai.ecma.appdblib.repository.product.SaleRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.enums.ProductFilterTypeEnum;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.feign.OrderFeign;
import ai.ecma.appproductservice.mapper.CustomMapper;
import ai.ecma.appproductservice.mapper.ProductMapper;
import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final AttachmentRepository attachmentRepository;
    private final SaleRepository saleRepository;
    private final ProductMapper productMapper;
    private final OrderFeign orderFeign;
    private final MessageByLang messageByLang;

    @Override
    public ApiResult<CustomPage<ProductResDto>> getAll(int page, int size, String field, boolean asc) {
        checkField(field);
        Pageable pageable = PageRequest.of(page, size, Sort.by(asc ? Sort.Direction.ASC : Sort.Direction.DESC, field));
        Page<Product> productPage = productRepository.findAll(pageable);
        CustomPage<ProductResDto> products = productToCustomPage(productPage);
        return ApiResult.successResponse(products);
    }

    @CheckAuth
    @Override
    public ApiResult<CustomPage<ProductResDto>> getFilter(ProductFilterTypeEnum type, UUID categoryId, boolean active, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Product> productPage = null;
        User user = CommonUtils.getUserFromRequest();
        switch (type) {
            case SALE:
                productPage = productRepository.findAllBySaleIdNotNull(pageable);
                break;
            case CATEGORY:
                if (categoryId == null)
                    throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("CATEGORY_ID_NOT_NULL"));
                productPage = productRepository.findAllByCategoryId(categoryId, pageable);
                break;
            case RECOMMENDED:
                if (categoryId == null)
                    throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("CATEGORY_ID_NOT_NULL"));
                productPage = productRepository.getAllRecProductForCategory(categoryId, pageable);
                break;
            case ACTIVE:
                productPage = productRepository.findAllByActive(active, pageable);
                break;
            case FAVORITE:
                productPage = productRepository.getAllByFavorite(user.getId(), pageable);
                break;
        }
        CustomPage<ProductResDto> products = productToCustomPage(productPage);
        return ApiResult.successResponse(products);
    }

    @Override
    public ApiResult<List<ProductResDto>> getByIds(Set<UUID> ids) {
        List<Product> products = productRepository.findAllByIdIsIn(ids);
        if (ids.size() != products.size())
            throw new RestException(HttpStatus.BAD_REQUEST,messageByLang.getMessageByKey("SOME_PRODUCTS_NOT_FOUND"));
        return ApiResult.successResponse(productMapper.listProductToDto(products));
    }

    @Override
    public ApiResult<ProductResDto> getOne(UUID id) {
        Product product = productRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));
        return ApiResult.successResponse(CustomMapper.productToDto(product));
    }

    @CheckAuth(permission = PermissionEnum.ADD_PRODUCT)
    @Override
    public ApiResult<ProductResDto> add(ProductReqDto productReqDto) {
        boolean existsByName = productRepository.existsByName(productReqDto.getName());
        if (existsByName)
            throw new RestException(HttpStatus.CONFLICT, messageByLang.getMessageByKey("EXISTED_PRODUCT"));
        Category category = categoryRepository.findById(productReqDto.getCategoryId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));
        if (category.getParentCategory() == null)
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ADDING_PRODUCT_TO_PARENT_CATEGORY_FORBIDDEN"));
        Attachment photo = attachmentRepository.findById(productReqDto.getPhotoId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ATTACHMENT_NOT_FOUND")));
        Product product = new Product(
                productReqDto.getName(),
                productReqDto.getPrice(),
                category,
                photo,
                productReqDto.getDescription(),
                productReqDto.getSaleId() != null ? saleRepository.findById(productReqDto.getSaleId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("SALE_NOT_FOUND")))
                        : null,
                productReqDto.getActive()
        );
        productRepository.save(product);
        return ApiResult.successResponse(CustomMapper.productToDto(product));
    }

    @CheckAuth(permission = PermissionEnum.EDIT_PRODUCT)
    @Override
    public ApiResult<ProductResDto> edit(UUID id, ProductReqDto productReqDto) {
        boolean existsByName = productRepository.existsByNameAndIdNot(productReqDto.getName(), id);
        if (existsByName)
            throw new RestException(HttpStatus.CONFLICT, messageByLang.getMessageByKey("EXISTED_PRODUCT"));
        Attachment photo = attachmentRepository.findById(productReqDto.getPhotoId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ATTACHMENT_NOT_FOUND")));
        Optional<Sale> optionalSale= saleRepository.findById(productReqDto.getSaleId());

        Product product = productRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND")));
        product.setName(productReqDto.getName());

        product.setSale(optionalSale.isEmpty() ? null : optionalSale.get());

        if (!product.getPrice().equals(productReqDto.getPrice())){
            updatePriceOrder(product,productReqDto, id);
        }
        product.setActive(productReqDto.getActive());
        product.setDescription(productReqDto.getDescription());
        product.setPhoto(photo);

        productRepository.save(product);
        return ApiResult.successResponse(CustomMapper.productToDto(product));
    }

    @CheckAuth(permission = PermissionEnum.DELETE_PRODUCT)
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            productRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("PRODUCT_DELETED"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PRODUCT_NOT_FOUND"));
        }
    }



    public CustomPage<ProductResDto> productToCustomPage(Page<Product> productPage) {
        return new CustomPage<ProductResDto>(
                productPage.getContent().stream().map(CustomMapper::productToDto).collect(Collectors.toList()),
                productPage.getNumberOfElements(),
                productPage.getNumber(),
                productPage.getTotalElements(),
                productPage.getTotalPages(),
                productPage.getSize()
        );
    }

    private void checkField(String field) {
        if (!(field.equals("name") || field.equals("price"))) {
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("WRONG_CHOICE_FOR_SORTING"));
        }
    }
    private void updatePriceOrder(Product product, ProductReqDto productReqDto, UUID id){
        product.setPrice(productReqDto.getPrice());
        Double salePrice;
        if (product.getSale()!=null){
            if (product.getSale().getType().equals(SaleTypeEnum.PERCENT)){
                salePrice = product.getPrice()*product.getSale().getAmount()/100;
            }else {
                salePrice = product.getSale().getAmount();
            }
        }else {
            salePrice =0d;
        }

        Thread editOrderProductPrice = new Thread(() ->
                orderFeign.updatePriceOrder(new OrderUpdatePriceDto(
                        id,
                        (product.getPrice()-salePrice))
                )
        );


        editOrderProductPrice.start();
    }

}
