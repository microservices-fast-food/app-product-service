package ai.ecma.appproductservice.payload;

import ai.ecma.appdblib.entity.product.Branch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BranchDistanceDto {
    private Branch branch;
    private Double distance;
}
