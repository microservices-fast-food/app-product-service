package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.product.Category;
import ai.ecma.appdblib.repository.product.CategoryRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.mapper.CustomMapper;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CategoryReqDto;
import ai.ecma.appproductservice.payload.CategoryResDto;
import ai.ecma.appproductservice.payload.CustomPage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final MessageByLang messageByLang;

    public CategoryServiceImpl(CategoryRepository categoryRepository, MessageByLang messageByLang) {
        this.categoryRepository = categoryRepository;
        this.messageByLang = messageByLang;
    }

    @Override
    public ApiResult<CustomPage<CategoryResDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Category> categoryPage = categoryRepository.findAll(pageable);
        CustomPage<CategoryResDto> categoryResDtoCustomPage = categoryToCustomPage(categoryPage);
        return ApiResult.successResponse(categoryResDtoCustomPage);
    }

    @Override
    public ApiResult<CategoryResDto> getOne(UUID id) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CATEGORY_NOT_FOUND")));
        return ApiResult.successResponse(CustomMapper.categoryToDto(category));
    }

    @CheckAuth(permission = PermissionEnum.ADD_CATEGORY)
    @Override
    public ApiResult<CategoryResDto> add(CategoryReqDto categoryReqDto) {
        if (categoryRepository.existsByName(categoryReqDto.getName()))
            throw new RestException(HttpStatus.CONFLICT, messageByLang.getMessageByKey("EXISTED_CATEGORY"));

        int index = categoryRepository.countByParentCategoryId(categoryReqDto.getParentCategoryId());
        Category category = new Category(
                categoryReqDto.getName(),
                categoryReqDto.getParentCategoryId() != null
                        ? categoryRepository.findById(categoryReqDto.getParentCategoryId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("PARENT_CATEGORY_NOT_FOUND")))
                        : null,
                (index + 1)
        );
        categoryRepository.save(category);
        return ApiResult.successResponse(CustomMapper.categoryToDto(category));
    }


    @CheckAuth(permission = PermissionEnum.EDIT_CATEGORY)
    @Override
    public ApiResult<CategoryResDto> edit(UUID id, CategoryReqDto categoryReqDto) {
        if (categoryRepository.existsByIdNotAndName(id, categoryReqDto.getName()))
            throw new RestException(HttpStatus.CONFLICT, messageByLang.getMessageByKey("EXISTED_CATEGORY"));
        Category category = categoryRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND,messageByLang.getMessageByKey("CATEGORY_NOT_FOUND")));
        if (!category.getIndex().equals(categoryReqDto.getIndex())) {
            int indexMin = categoryReqDto.getIndex() < category.getIndex() ? categoryReqDto.getIndex() : category.getIndex();
            int indexMax = categoryReqDto.getIndex() > category.getIndex() ? categoryReqDto.getIndex() : category.getIndex();
            List<Category> categoryList = null;
            if (category.getParentCategory() == null) {
                categoryList = categoryRepository.getByParentCategoryIsNullBetween(indexMin, indexMax);
            } else {
                categoryList = categoryRepository.getByParentCategoryBetween(category.getParentCategory().getId(), indexMin, indexMax);
            }

            boolean tushish = category.getIndex() < categoryReqDto.getIndex();
            for (Category category1 : categoryList) {
                category1.setIndex(tushish ? (category1.getIndex() - 1)
                        : (category1.getIndex() + 1));
            }
            categoryRepository.saveAll(categoryList);
            category.setIndex(categoryReqDto.getIndex());
        }

        category.setName(categoryReqDto.getName());
        categoryRepository.save(category);

        return ApiResult.successResponse(CustomMapper.categoryToDto(category));
    }

    @CheckAuth(permission = PermissionEnum.DELETE_CATEGORY)
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            categoryRepository.deleteById(id);
                return ApiResult.successResponse(messageByLang.getMessageByKey("CATEGORY_DELETED"));
        } catch (Exception e) {
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("CATEGORY_NOT_FOUND"));
        }
    }

    public CustomPage<CategoryResDto> categoryToCustomPage(Page<Category> categoryPage) {
        return new CustomPage<CategoryResDto>(
                categoryPage.getContent().stream().map(CustomMapper::categoryToDto).collect(Collectors.toList()),
                categoryPage.getNumberOfElements(),
                categoryPage.getNumber(),
                categoryPage.getTotalElements(),
                categoryPage.getTotalPages(),
                categoryPage.getSize()
        );
    }
}
