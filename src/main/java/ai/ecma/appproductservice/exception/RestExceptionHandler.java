package ai.ecma.appproductservice.exception;


import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.ErrorData;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RequiredArgsConstructor
@RestControllerAdvice
public class RestExceptionHandler {
    private final MessageByLang messageByLang;



    /**
     * Bu restException da 2 xil xolat bo'yicha ya'ni errorData lar uchun va alohida HTTP message va ststus qaytarish
     * uchun qildek. Error Data larni qaytaradigani CustomErrorDecoder uchun maxsus tayuyorlandi.
     * @param restException
     * @return
     */
    @ExceptionHandler(value = RestException.class)
    public ResponseEntity<?> exceptionHandling(RestException restException) {
        restException.printStackTrace();
        if (restException.getErrorData()!=null&&!restException.getErrorData().isEmpty())
            return ResponseEntity.status(restException.getStatus())
                    .body(ApiResult.errorResponse(restException.getErrorData()));

        return ResponseEntity.
                status(restException.getStatus())
                .body(ApiResult.errorResponse(restException.getMessage(),restException.getObject()));
    }


    //Bu validatsiyadan o'tolmagan body uchun
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<?> exceptionHandling(MethodArgumentNotValidException e){
        List<ErrorData> errorData = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(objectError -> errorData.add(
                new ErrorData(objectError.getDefaultMessage(), 400)
                )
        );
        return ResponseEntity.status(400).body(ApiResult.errorResponse(errorData));
    }


    //bu internal server error
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> exceptionHandling(Exception e){
        e.printStackTrace();
        return ResponseEntity
                .status(500)
                .body(ApiResult.errorResponse("Internal server error"));
    }

    //body bermasa yoki parse qilomasa
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<?> exceptionHandling(HttpMessageNotReadableException ex) {
        ex.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ApiResult.errorResponse("Parametr xato tipda berildi!"));
    }

    /**
     * BU FOREIGN KEY LARNI TOPOLMAGANDA BERUVCHI EXCEPTION UNI ICHIDAN
     * COLUMN NAME NI OLIB CLIENTGA QAYTARADI
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    public ResponseEntity<?> handleException(DataIntegrityViolationException ex) {
        ex.printStackTrace();
        try {
            SQLException sqlException = ((ConstraintViolationException) ex.getCause()).getSQLException();
            String message = sqlException.getMessage();
            String detail = "Detail:";

            //DETAIL: SO'ZINI INDEKSINI ANIQLAB OLYAPMAN ARENTIR SIFATIDA
            int arentir = message.indexOf(detail);

            //DETAIL SO'ZIDAN KEYINGI OCHILGAN 1-QAVS NI INDEXINI OLIB +1 QO'SHTIM
            int fromColumnName = message.indexOf("(", arentir) + 1;

            //DETAIL SO'ZIDAN KEYINGI YOPILGAN 1-QAVS NI INDEXINI OLIB -3 AYIRDIM
            int toColumnName = message.indexOf(")", fromColumnName) - 3;

            //MESSAGEDAN COLUMN NAME NI QIRQIB OLIB UNI UPPER_CASE QILINDI
            String columnName = message.substring(fromColumnName, toColumnName).toUpperCase(Locale.ROOT);

            //MESSAGE_BY_LANG GA BERISH UCHUN
            String clientMessage = columnName + "_NOT_FOUND";
            return new ResponseEntity<>(
                    ApiResult.errorResponse(messageByLang.getMessageByKey(clientMessage)),
                    HttpStatus.NOT_FOUND
            );
        } catch (Exception exception) {
            exception.printStackTrace();
            return new ResponseEntity<>(
                    ApiResult.errorResponse("Server error. Please try again"),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
