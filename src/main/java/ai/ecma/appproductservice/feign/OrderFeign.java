package ai.ecma.appproductservice.feign;


import ai.ecma.appproductservice.config.FeignConfig;
import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.OrderUpdatePriceDto;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = RestConstants.ORDER_CONTROLLER, configuration = FeignConfig.class)
public interface OrderFeign {

    @PostMapping("/update-price-order-product")
    ApiResult<?> updatePriceOrder(@RequestBody OrderUpdatePriceDto orderUpdatePriceDto);




}
