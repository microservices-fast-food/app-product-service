package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Time;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkDayEditReqDto {

    @NotNull(message = "{START_TIME_NOT_NULL}")
    private Time startTime;

    @NotNull(message = "{END_TIME_NOT_NULL}")
    private Time endTime;
}
