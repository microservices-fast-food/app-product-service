package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class BranchProductResDto {

    private UUID id;

    private UUID branchId;

    private UUID productId;

    private Boolean active;

}
