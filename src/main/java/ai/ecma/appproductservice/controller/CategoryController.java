package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CategoryReqDto;
import ai.ecma.appproductservice.payload.CategoryResDto;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.utils.AppConstant;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstants.CATEGORY_CONTROLLER)
public interface CategoryController {

    @GetMapping()
    ApiResult<CustomPage<CategoryResDto>> getAll(
            @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("/{id}")
    ApiResult<CategoryResDto> getOne(@PathVariable UUID id);

    @PostMapping
    ApiResult<CategoryResDto> add(@RequestBody @Valid CategoryReqDto categoryReqDto);

    @PutMapping("/{id}")
    ApiResult<CategoryResDto> edit(@PathVariable UUID id, @RequestBody @Valid CategoryReqDto categoryReqDto);

    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}