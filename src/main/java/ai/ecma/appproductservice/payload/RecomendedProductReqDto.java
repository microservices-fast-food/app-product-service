package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecomendedProductReqDto {
    @NotNull(message = "{CATEGORY_ID_NOT_NULL}")
    private UUID categoryId;

    @NotNull(message = "{PRODUCT_ID_NOT_NULL}")
    private UUID productId;
}
