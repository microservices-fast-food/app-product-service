package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.utils.AppConstant;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstants.BRANCH_PRODUCT_CONTROLLER)
public interface BranchProductController {

    @GetMapping
    ApiResult<CustomPage<BranchProductResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                      @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("{id}")
    ApiResult<BranchProductResDto> getOne(@PathVariable UUID id);

    @PostMapping
    ApiResult<BranchProductResDto> add(@RequestBody @Valid BranchProductReqDto branchProductReqDto);

    @PutMapping("{id}")
    ApiResult<BranchProductResDto> edit(@RequestBody @Valid BranchProductEditDto branchProductEditDto, @PathVariable UUID id);

    @DeleteMapping("{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
