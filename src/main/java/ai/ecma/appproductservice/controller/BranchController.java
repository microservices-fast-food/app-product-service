package ai.ecma.appproductservice.controller;

import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.utils.AppConstant;
import ai.ecma.appproductservice.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstants.BRANCH_CONTROLLER)
public interface BranchController {

    @GetMapping("/byDistrict")
    ApiResult<List<BranchResDto>> getAllByDistrictId();

    @GetMapping
    ApiResult<CustomPage<BranchResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                               @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @GetMapping("{id}")
    ApiResult<BranchResDto> getBranch(@PathVariable UUID id);

    @PostMapping()
    ApiResult<BranchResDto> add(@RequestBody @Valid BranchReqDto branchReqDto);

    @PutMapping("{id}")
    ApiResult<BranchResDto> edit(@RequestBody @Valid BranchReqDto branchReqDto, @PathVariable UUID id);

    @DeleteMapping("{id}")
    ApiResult<?> delete(@PathVariable UUID id);

    /**
     * Shartlar:
     * 1.Biz bergan address ga eng yaqin branchlar
     * 2.Biz bergan product lar shu branch larda bo'lsin
     * 3.Hozirgi vaqtda ushbu branchlar ish faolyatida bo'lsin
     * @param findNearestBranchDto
     * @return
     */
    @PostMapping("/find-nearest-branchs")
    ApiResult<List<BranchInfoForClientDto>> findNearestBranchs(@RequestBody FindNearestBranchDto findNearestBranchDto);

    /**
     * Shartlar:
     * 1.Biz bergan address ga eng yaqin branch
     * 2.Biz bergan product lar shu branch da bo'lsin
     * 3.Hozirgi vaqtda ushbu branch ish faolyatida bo'lsin
     * @param findNearestBranchDto
     * @return
     */
    @PostMapping("/find-nearest-branch")
    ApiResult<BranchInfoForClientDto> findNearestBranch(@RequestBody FindNearestBranchDto findNearestBranchDto);
}
