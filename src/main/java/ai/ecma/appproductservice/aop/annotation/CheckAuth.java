package ai.ecma.appproductservice.aop.annotation;


import ai.ecma.appdblib.entity.enums.PermissionEnum;

import java.lang.annotation.*;


@Retention(value = RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface CheckAuth {
    PermissionEnum permission() default PermissionEnum.CHECK;
}
