package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BranchProductEditDto {
    @NotNull(message = "{ACTIVE_VALUE_NOT_NULL}")
    private Boolean active;
}
