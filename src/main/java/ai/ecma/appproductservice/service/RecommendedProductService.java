package ai.ecma.appproductservice.service;

import ai.ecma.appproductservice.payload.ApiResult;
import ai.ecma.appproductservice.payload.CustomPage;
import ai.ecma.appproductservice.payload.RecomendedProductReqDto;
import ai.ecma.appproductservice.payload.RecomendedProductResDto;

import java.util.List;
import java.util.UUID;

public interface RecommendedProductService {
    ApiResult<CustomPage<RecomendedProductResDto>> getAll(int page, int size);

    ApiResult<RecomendedProductResDto> getOne(UUID id);

    ApiResult<List<RecomendedProductResDto>> getByCategory(UUID categoryId);

    ApiResult<RecomendedProductResDto> add(RecomendedProductReqDto recomendedProductReqDto);

    ApiResult<?> delete(UUID id);
}
