package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.enums.WeekdayEnum;
import ai.ecma.appdblib.entity.product.Branch;
import ai.ecma.appdblib.entity.product.WorkDay;
import ai.ecma.appdblib.repository.product.BranchRepository;
import ai.ecma.appdblib.repository.product.WorkDayRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.mapper.CustomMapper;
import ai.ecma.appproductservice.payload.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class WorkDayServiceImpl implements WorkDayService {

    private final WorkDayRepository workDayRepository;
    private final BranchRepository branchRepository;
    private final MessageByLang messageByLang;

    @CheckAuth(permission = PermissionEnum.VIEW_WORKDAY)
    @Override
    public ApiResult<CustomPage<WorkDaysResDto>> getAll(int page, int size) {
        Pageable pageable= PageRequest.of(page, size);
        Page<WorkDay> workDays = workDayRepository.findAll(pageable);
        CustomPage<WorkDaysResDto> workDaysResDtoCustomPage=workDaysResDtoCustomPage(workDays);
        return ApiResult.successResponse(workDaysResDtoCustomPage);
    }

    @CheckAuth(permission = PermissionEnum.VIEW_WORKDAY)
    @Override
    public ApiResult<WorkDaysResDto> getOne(UUID id) {
        WorkDay workDay = workDayRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("WORKDAY_NOT_FOUND")));
        return ApiResult.successResponse(CustomMapper.workDaysToDto(workDay));
    }

    @CheckAuth(permission = PermissionEnum.ADD_WORKDAY)
    @Override
    public ApiResult<WorkDaysResDto> add(WorkDaysReqDto workDaysReqDto) {
        if (workDaysReqDto.getStartTime().after(workDaysReqDto.getEndTime()))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ERROR_TIME"));
        List<Branch> branchList = branchRepository.findAllById(workDaysReqDto.getBranchIdList());
        Set<WeekdayEnum> weekdayList = workDaysReqDto.getWeekdayList();
        if (branchList.size()!=workDaysReqDto.getBranchIdList().size())
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ERROR_BRANCH_OR_WEEKDAY"));
        List<WorkDay> workDayList=new ArrayList<>();
        for (Branch branch : branchList) {
            for (WeekdayEnum weekdayEnum : weekdayList) {
                workDayList.add(
                        new WorkDay(
                                branch,
                                weekdayEnum,
                                workDaysReqDto.getStartTime(),
                                workDaysReqDto.getEndTime()
                        )
                );
            }
        }
        workDayRepository.saveAll(workDayList);
        return ApiResult.successResponse(messageByLang.getMessageByKey("WORKDAYS_ADDED"));
//        Branch branch = branchRepository.findById(workDaysReqDto.getBranchId()).orElseThrow(()
//                -> new RestException(HttpStatus.NOT_FOUND, "Filial topilmadi"));
//        WorkDay workDay=new WorkDay(
//                branch,
//                workDaysReqDto.getWeekday(),
//                workDaysReqDto.getStartTime(),
//                workDaysReqDto.getEndTime()
//        );
//        workDayRepository.save(workDay);
//        return ApiResult.successResponse(CustomMapper.workDaysToDto(workDay));
    }

    @CheckAuth(permission = PermissionEnum.EDIT_WORKDAY)
    @Override
    public ApiResult<WorkDaysResDto> edit(WorkDayEditReqDto workDayEditReqDto, UUID id) {
        if (workDayEditReqDto.getStartTime().after(workDayEditReqDto.getEndTime()))
            throw new RestException(HttpStatus.BAD_REQUEST, messageByLang.getMessageByKey("ERROR_TIME"));
        WorkDay workDay = workDayRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("WORKDAY_NOT_FOUND")));
        workDay.setStartTime(workDayEditReqDto.getStartTime());
        workDay.setEndTime(workDayEditReqDto.getEndTime());
        return ApiResult.successResponse(CustomMapper.workDaysToDto(workDay));
    }
    @CheckAuth(permission = PermissionEnum.DELETE_WORKDAY)
    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            workDayRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("WORKDAY_DELETED"));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("WORKDAY_NOT_FOUND"));
    }

    public CustomPage<WorkDaysResDto> workDaysResDtoCustomPage(Page<WorkDay> workDayPage){
        return new CustomPage<>(
                workDayPage.getContent().stream().map(CustomMapper::workDaysToDto).collect(Collectors.toList()),
                workDayPage.getTotalPages(),
                workDayPage.getNumber(),
                workDayPage.getTotalElements(),
                workDayPage.getSize(),
                workDayPage.getNumberOfElements()
        );
    }
}
