package ai.ecma.appproductservice.payload;

import ai.ecma.appdblib.entity.enums.SaleTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleReqDTO {

    @NotEmpty(message = "{SALE_NAME_NOT_EMPTY}")
    private String name;

    @NotNull(message = "{SALE_TYPE_NOT_NULL}")
    private SaleTypeEnum type;

    private Boolean active;

    @NotNull(message = "{SALE_AMOUNT_NOT_NULL}")
    private Double amount;
}
