package ai.ecma.appproductservice.payload;

import ai.ecma.appdblib.entity.enums.SaleTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleResDTO {

    private UUID id;

    private String name;

    private SaleTypeEnum type;

    private Boolean active;

    private Double amount;
}
