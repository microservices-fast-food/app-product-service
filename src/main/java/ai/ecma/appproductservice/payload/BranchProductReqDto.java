package ai.ecma.appproductservice.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class BranchProductReqDto {

    @NotNull(message = "{BRANCH_ID_NOT_NULL}")
    private List<UUID> branchIdList;

    @NotNull(message = "{PRODUCT_ID_NOT_NULL}")
    private List<UUID> productIdList;

    @NotNull(message = "{ACTIVE_VALUE_NOT_NULL}")
    private Boolean active;
}
