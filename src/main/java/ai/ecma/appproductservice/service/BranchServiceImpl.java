package ai.ecma.appproductservice.service;

import ai.ecma.appdblib.entity.enums.PermissionEnum;
import ai.ecma.appdblib.entity.product.Branch;
import ai.ecma.appdblib.entity.product.DeliveryPrice;
import ai.ecma.appdblib.entity.user.User;
import ai.ecma.appdblib.repository.product.BranchRepository;
import ai.ecma.appdblib.repository.product.DeliveryPriceRepository;
import ai.ecma.appproductservice.aop.annotation.CheckAuth;
import ai.ecma.appproductservice.component.MessageByLang;
import ai.ecma.appproductservice.exception.RestException;
import ai.ecma.appproductservice.feign.AuthFeign;
import ai.ecma.appproductservice.mapper.CustomMapper;
import ai.ecma.appproductservice.payload.*;
import ai.ecma.appproductservice.utils.CommonUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final DeliveryPriceRepository deliveryPriceRepository;

    private final AuthFeign authFeign;
    private final MessageByLang messageByLang;

    @Override
    public ApiResult<CustomPage<BranchResDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "name"));
        Page<Branch> branchPage = branchRepository.findAll(pageable);
        CustomPage<BranchResDto> customResPage = branchResDtoCustomPage(branchPage);
        return ApiResult.successResponse(customResPage);
    }

    @Override
    public ApiResult<BranchResDto> getBranch(UUID id) {
        Branch branch = branchRepository.findById(id)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BRANCH_NOT_FOUND")));
        return ApiResult.successResponse(CustomMapper.branchResToDoDto(branch));
    }

    @CheckAuth(permission = PermissionEnum.ADD_BRANCH)
    @Override
    public ApiResult<BranchResDto> addBranch(BranchReqDto branchReqDto) {
        int n = 0;
        AddressResDto addressResDto = authFeign.saveAddress(branchReqDto.getAddressReqDto()).getData();
        Branch branch = new Branch(
                branchReqDto.getName(),
                addressResDto.getId(),
                branchReqDto.getActive(),
                branchReqDto.getCookingTime(),
                branchReqDto.getAutoDelivered(),
                branchReqDto.getDeliveryMaxKm()
        );
        Branch branch1 = branchRepository.save(branch);
        return ApiResult.successResponse(CustomMapper.branchResToDoDto(branch1));
    }

    @CheckAuth(permission = PermissionEnum.EDIT_BRANCH)
    @Override
    public ApiResult<BranchResDto> editBranch(BranchReqDto branchReqDto, UUID id) {
        Branch branch = branchRepository.findById(id).orElseThrow(()
                -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BRANCH_NOT_FOUND")));
        AddressResDto addressResDto = authFeign.editAddress(
                branchReqDto.getAddressReqDto(),
                branch.getAddressId()).getData();
        branch.setName(branchReqDto.getName());
        branch.setAddressId(addressResDto.getId());
        branch.setActive(branchReqDto.getActive());
        branch.setCookingTime(branchReqDto.getCookingTime());
        branch.setAutoDelivery(branchReqDto.getAutoDelivered());
        branch.setDeliveryMaxKm(branchReqDto.getDeliveryMaxKm());
        branchRepository.save(branch);
        return ApiResult.successResponse(CustomMapper.branchResToDoDto(branch));
    }

    @CheckAuth(permission = PermissionEnum.DELETE_BRANCH)
    @Override
    public ApiResult<?> deleteBranch(UUID id) {
        try {
            branchRepository.deleteById(id);
            return ApiResult.successResponse(messageByLang.getMessageByKey("BRANCH_DELETED"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("BRANCH_NOT_FOUND"));
        }
    }

    @CheckAuth
    @Override
    public ApiResult<List<BranchResDto>> getAllByDistrictId() {
        User user = CommonUtils.getUserFromRequest();
        List<Branch> branchByDistrictId = branchRepository.getBranchByDistrictId(user.getDistrict().getId());
        return ApiResult.successResponse(branchByDistrictId.stream().map(CustomMapper::branchResToDoDto).collect(Collectors.toList()));
    }

    @SneakyThrows
    @Override
    public ApiResult<List<BranchInfoForClientDto>> findNearestBranchs(FindNearestBranchDto findNearestBranchDto) {
        //metod biz bergan vaqt va kunda ishlayotgan hamda order product lari shu branchda mavjud bo'lgan branchlarni
        // order address igacha bo'lgan masofasini hisoblab o'sish tartibida saralab branchlarni qaytaradi
        List<BranchInfoForClientDto> branchDistanceDtos = getSortByDistanceBranchesByActiveHasProducts(findNearestBranchDto);

        return ApiResult.successResponse(branchDistanceDtos);
    }

    @Override
    public ApiResult<BranchInfoForClientDto> findNearestBranch(FindNearestBranchDto findNearestBranchDto) {
        //metod biz bergan vaqt va kunda ishlayotgan hamda order product lari shu branchda mavjud bo'lgan branchlarni
        // order address igacha bo'lgan masofasini hisoblab o'sish tartibida saralab branchlarni qaytaradi
        List<BranchInfoForClientDto> branchDistanceDtos = getSortByDistanceBranchesByActiveHasProducts(findNearestBranchDto);

        BranchInfoForClientDto branchInfoForClientDto = branchDistanceDtos.get(0);

        //metod branchlarni addresini order address gacha distance  delivery price ni hisoblaydi
        calculateDeliveryPrice(branchInfoForClientDto);


        return ApiResult.successResponse(branchInfoForClientDto);
    }


    public CustomPage<BranchResDto> branchResDtoCustomPage(Page<Branch> branchePage) {
        return new CustomPage<>(
                branchePage.getContent().stream().map(CustomMapper::branchResToDoDto).collect(Collectors.toList()),
                branchePage.getTotalPages(),
                branchePage.getNumber(),
                branchePage.getTotalElements(),
                branchePage.getSize(),
                branchePage.getNumberOfElements()
        );
    }

    //branch lar listidan addresId va branch bo'lgan HashMap yasab oldik
    private HashMap<UUID, Branch> getAddressBranchMap(List<Branch> branchList) {
        HashMap<UUID, Branch> addressBranchMap = new HashMap<>();
        for (Branch branch : branchList) {
            addressBranchMap.put(branch.getAddressId(), branch);
        }
        return addressBranchMap;
    }

    //oraliq masofani hisoblash
    public double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        dist = dist * 1.609344;

        return (dist);
    }

    //distance niki
    public double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    //distance niki
    public double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    //feign orali olib kelgan addreslarimizdan client address ini olish
    private AddressForBranchDto getClientAddress(List<AddressForBranchDto> addresses, UUID clientAddressId) {
        for (AddressForBranchDto address : addresses) {
            if (address.getId().equals(clientAddressId))
                return address;
        }
        throw new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("ADDRESS_NOT_FOUND"));
    }

    //branch listni distance bo'yicha o'shir tartibida saralab beradi
    private void sortingListByDistance(List<BranchInfoForClientDto> branchDistanceDtos) {
        branchDistanceDtos.sort(Comparator.comparing(BranchInfoForClientDto::getDistance));
    }

    //metod branchlarni addresini order address gacha distance  delivery price ni hisoblaydi
    private void calculateDeliveryPrice(BranchInfoForClientDto branchInfoForClientDto ){

        DeliveryPrice deliveryPrice = deliveryPriceRepository.findByBranchId(branchInfoForClientDto.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, messageByLang.getMessageByKey("DELIVERY_PRICE_NOT_FOUND")));
        if (branchInfoForClientDto.getId().equals(deliveryPrice.getBranch().getId())){
            Double deliveryPriceSum;
            Double distance = branchInfoForClientDto.getDistance();
            if (distance <= deliveryPrice.getMinKm())
                deliveryPriceSum = deliveryPrice.getMinPrice();
            else
                deliveryPriceSum = deliveryPrice.getPriceEveryKm()*distance;

            branchInfoForClientDto.setDeliveryPrice(deliveryPriceSum);
        }

    }

    //metod biz bergan vaqt va kunda ishlayotgan hamda order product lari shu branchda mavjud bo'lgan branchlarni order address igacha bo'lgan masofasini hisoblab o'sish tartibida saralab branchlarni qaytaradi
    private List<BranchInfoForClientDto> getSortByDistanceBranchesByActiveHasProducts(FindNearestBranchDto findNearestBranchDto){
        Timestamp time = CommonUtils.buildTimeStampInTime(findNearestBranchDto.getPlanReceivedTime());
        String weekday = LocalDate.now().getDayOfWeek().name();
        List<Branch> branches = branchRepository.getBranchByWorkingAndHasProducts(findNearestBranchDto.getProductIds(), weekday, time);
        Set<UUID> addressIds = new HashSet<>();
        for (Branch branch : branches) {
            addressIds.add(branch.getAddressId());
        }
        addressIds.add(findNearestBranchDto.getAddressId());

        //auth service dan id lar orqali address larni qayataradi
        List<AddressForBranchDto> addressForBranchDtoList = authFeign.getAddressesByIds(addressIds).getData();

        AddressForBranchDto clientAddress = getClientAddress(addressForBranchDtoList, findNearestBranchDto.getAddressId());
        HashMap<UUID, Branch> addressBranchMap = getAddressBranchMap(branches);
        List<BranchInfoForClientDto> branchDistanceDtos = new ArrayList<>();
        for (AddressForBranchDto addressForBranchDto : addressForBranchDtoList) {
            double distance = distance(
                    clientAddress.getLat(),
                    clientAddress.getLon(),
                    addressForBranchDto.getLat(),
                    addressForBranchDto.getLon()
            );
            Branch branch = addressBranchMap.get(addressForBranchDto.getId());
            if (distance > branch.getDeliveryMaxKm())
                branches.remove(branch);

            branchDistanceDtos.add(new BranchInfoForClientDto(
                    branch.getId(),
                    distance,
                    null,
                    branch.getCookingTime()
            ));
        }

        //branch listni distance bo'yicha o'shir tartibida saralab beradi
        sortingListByDistance(branchDistanceDtos);

        return branchDistanceDtos;
    }
}
